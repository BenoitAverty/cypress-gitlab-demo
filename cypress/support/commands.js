// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add("gitlabLogin", (username, password) => {
  cy.visit("https://gitlab.com/users/sign_in");
  cy.selectByLabel("Remember me", 'checkbox').check()
  cy.selectByLabel("Username or email").type(username);
  cy.selectByLabel("Password", 'password').focus();
  cy.get('a.header-user-dropdown-toggle', { timeout: 10000 }).should('have.attr', 'href', '/BenoitAverty')
});

Cypress.Commands.add("selectByLabel", (label, type="text") => {
  cy.contains("label", label)
    .parent()
    .find(`input[type="${type}"]`);
});
