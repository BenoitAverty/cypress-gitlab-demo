describe("Issues Feature", () => {
  before(() => {
    cy.clearCookies();
    cy.gitlabLogin("BenoitAverty");
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce("remember_user_token");
    cy.visit("https://gitlab.com/BenoitAverty/cypress-gitlab-demo/issues");
  });

  it("Creating a simple issue", () => {
    cy.contains(".btn", "New issue")
      .should("be.visible")
      .click();

    let issueTitle =
      "Faire marcher l'authentication des tests en mode headless";
    cy.selectByLabel("Title").type(issueTitle);

    cy.get("#issue_description")
      .type(`Actuellement, la commande cypress \`gitlabLogin\` nécéssite de saisir le mot de passe à la main.
      
Ceci est impossible quand les tests sont lancés en dehors du runner (en particulier dans la ci).

Il faudrait trouver une autre solution pour saisir le mot de passe de manière sécurisée (Variable d'env ?)`);

    cy.get(".qa-assign-to-me-link")
      .scrollIntoView()
      .click();

    cy.contains('input[type="submit"]', "Submit issue").click();

    cy.contains("h2.qa-title", issueTitle);
    cy.get(".qa-assignee-block .author").contains("Benoit Averty");
  });

  it("Subscribing / Unsubscribing to an issue", () => {
    cy.server()
    cy.route({
      method: "POST",
      url: "/BenoitAverty/cypress-gitlab-demo/issues/4/toggle_subscription",
      status: 500,
      response: ""
    });

    cy.contains("Issue de démo").click();
    cy.contains("Notifications").parent().find('button').click();
    cy.contains('.flash-text', "Error occurred when toggling the notification subscription")
  });
});
